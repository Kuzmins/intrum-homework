Small homework application.  
App use in memory database so after restart all data will be erased.  
To run app:  
#With gradle installed:  
```
gradle build bootRun
```
#Gradle wrapper:  
```
gradlew build bootRun  
```

or for windows  
```
gradlew.bat build bootRun    
```

Rest end points  
```
POST http://localhost:8085/rest/v1/client  - register client

Request body: 
{  
   "name":"Germans",
   "surname":"Kuzmins",
   "country":"Latvia",
   "email":"test2@test.com",
   "password":"test23232"
}

Response body:
{
    "name": "Germans",
    "surname": "Kuzmins",
    "country": "Latvia",
    "email": "test2@test.com"
}

```
```

POST http://localhost:8085/auth/signin  - sign in to get access token
Request body:
{  
   "email":"test2@test.com",
   "password":"test23232"
}

Response body:
{
    "email": "test2@test.com",
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0MkB0ZXN0LmNvbSIsInJvbGVzIjpbIlVTRVIiXSwiaWF0IjoxNTUyMjUyNjc1LCJleHAiOjE1NTIyNTYyNzV9.9RoiR6oEW8A1QeDYRhStJbrIqlM-YrzcbaSVz9moH7o"
}


```

```
GET http://localhost:8085/rest/v1/client/  - get client basic info
Headers: 
Authorization: Bearer <token from /auth/signin resposne>

response body:
{
    "name": "Germans",
    "surname": "Kuzmins",
    "country": "Latvia",
    "email": "test@test.com"
}
```

```
GET http://localhost:8085/rest/v1/client/detailed  - get client detailed info
Headers: 
Authorization: Bearer <token from /auth/signin resposne>

response body:
{
    "name": "Germans",
    "surname": "Kuzmins",
    "country": "Latvia",
    "email": "test@test.com",
    "population": 1961600,
    "area": 64559,
    "borders": [
        "BLR",
        "EST",
        "LTU",
        "RUS"
    ]
}
```
```
Project using Lombok and MapStruct libraries need to enable annotation processing if no enabled:
https://immutables.github.io/apt.html
```

