package lv.intrum.homework.countryresolver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestCountryFetcherTest {

    private RestCountryFetcher restCountryFetcher = new RestCountryFetcher("https://restcountries.eu/rest/v2/");

    @Test
    public void shouldResponseCountryData() {
        List<RestCountryResponse> latvia = restCountryFetcher.getCountryData("latvia");
        assertThat(latvia.size(), is(1));
        assertThat(latvia.get(0).getName(), is("Latvia"));
    }

    @Test
    public void shouldReturnAllCountryNames() {
        List<String> countryNames = restCountryFetcher.getCountryList();
        assertThat(countryNames.size(), is(250));
    }
}