package lv.intrum.homework.countryresolver

import lv.intrum.homework.Success
import org.mapstruct.factory.Mappers
import spock.lang.Specification

import java.util.stream.Collectors
import java.util.stream.IntStream

class CountryResolverTest extends Specification {

    def countryFetcher = Mock(RestCountryFetcher)
    def countryMapper = Mappers.getMapper(RestCountryMapper.class);

    def countryResolver = new CountryResolver(countryFetcher, countryMapper)

    def "should throw exception if name not passed"() {
        given:
        countryFetcher.getCountryData(_ as String) >> getCountryData(1)
        countryFetcher.getCountryList() >> ["some country"]

        when:
        countryResolver.getCountryData(countryName)
        throw new Success()

        then:
        def error = thrown(expectedException)
        error.message == expectedMessage

        where:
        countryName | expectedException        | expectedMessage
        ""          | IllegalArgumentException | "CountryData name is mandatory"
        null        | IllegalArgumentException | "CountryData name is mandatory"
        "someName"  | Success                  | null
    }

    def "should return empty list if country not found and do not invoke second request"() {
        given:
        countryFetcher.getCountryData(_ as String) >> getCountryData(1)
        countryFetcher.getCountryList() >> returnedCountry

        when:
        countryResolver.getCountryData("country")

        then:
        countryFetchInokve * countryFetcher.getCountryData("country")

        where:
        returnedCountry          | countryFetchInokve
        []                       | 0
        ["some another country"] | 0
        ["country"]              | 1
    }

    private static List<CountryData> getCountryData(int count) {
        return IntStream.rangeClosed(1, count).mapToObj({ new CountryData() }).collect(Collectors.toList())
    }

    private static List<String> getCountryNames(int count) {
        return IntStream.rangeClosed(1, count).mapToObj({ "some country ${it}" }).collect(Collectors.toList())
    }
}
