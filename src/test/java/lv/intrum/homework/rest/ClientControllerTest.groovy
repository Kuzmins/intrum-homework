package lv.intrum.homework.rest

import com.fasterxml.jackson.databind.ObjectMapper
import lv.intrum.homework.client.ClientAlreadyExistException
import lv.intrum.homework.client.ClientService
import lv.intrum.homework.client.DetailedClient
import lv.intrum.homework.client.WrongCountryException
import org.mapstruct.factory.Mappers
import org.springframework.http.MediaType
import org.springframework.security.core.context.SecurityContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.hamcrest.Matchers.hasSize
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ClientControllerTest extends Specification {

    private MockMvc mvc
    private ObjectMapper objectMapper
    def clientService = Mock(ClientService)
    def clientRestMapper = Mappers.getMapper(ClientRestMapper.class)
    def securityContext = Stub(SecurityContext)


    void setup() {
        mvc = MockMvcBuilders
                .standaloneSetup(new ClientController(clientService, clientRestMapper))
                .build()
        objectMapper = new ObjectMapper()
    }

    def "should return 401 if wrong request"() {
        given:
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest()
                .setName(testName)
                .setSurname(testSurname)
                .setCountry(testCountry)
                .setEmail(testEmail)
                .setPassword(testPassword)

        when:
        def result = mvc.perform(post("/rest/v1/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRegistrationRequest)))
                .andExpect(status().is(statusCode))
                .andReturn()

        then:
        result != null


        where:
        testName | testSurname | testCountry | testEmail       | testPassword    | statusCode
        ""       | ""          | ""          | ""              | ""              | 400
        null     | null        | null        | null            | null            | 400
        "Bob"    | "Richards"  | "a"         | "a"             | "a"             | 400
        "Bob"    | "Richards"  | "Latvia"    | "adasdasd23123" | "a"             | 400
        "Bob"    | "Richards"  | "Latvia"    | "test@test.com" | "a"             | 400
        "Bob"    | "Richards"  | "Latvia"    | "test@test.com" | "strongPass123" | 201
    }

    def "should return 400 if country not found and provide list of countries"() {
        given:
        def clientRequest = testRequest()
        def availableCountries = ["Latvia", "USA"]
        clientService.registerClient(_ as ClientRegistrationRequest) >>
                { throw new WrongCountryException("wrong country", availableCountries) }

        when:
        def result = mvc.perform(post("/rest/v1/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath('$.errorMessage').value("wrong country"))
                .andExpect(MockMvcResultMatchers.jsonPath('$.availableCountries').value(hasSize(2)))
                .andReturn()

        then:
        result != null
    }

    def "should return saved client basic info after registration"() {
        given:
        def clientRequest = testRequest()
        def detailedClient = new DetailedClient()
        detailedClient.name = testRequest().getName()
        clientService.registerClient(_ as ClientRegistrationRequest) >> detailedClient

        when:
        def result = mvc.perform(post("/rest/v1/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isCreated())
                .andReturn()

        then:
        def response = result.getResponse().getContentAsString()
        def createdClient = objectMapper.readValue(response, ClientBasicInfo.class)

        createdClient.name == clientRequest.getName()
    }

    def "should return 409 if client already with email exist"() {
        given:
        def clientRequest = testRequest()
        clientService.registerClient(_ as ClientRegistrationRequest) >>
                { throw new ClientAlreadyExistException("clientAlready exist") }

        when:
        def result = mvc.perform(post("/rest/v1/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isConflict())
                .andReturn()

        then:
        result != null
    }

    private static ClientRegistrationRequest testRequest() {
        return new ClientRegistrationRequest()
                .setName("Bob")
                .setSurname("Richards")
                .setPassword("testPass123")
                .setCountry("Latvia")
                .setEmail("test@test.com")
    }
}
