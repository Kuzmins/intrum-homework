package lv.intrum.homework

import com.fasterxml.jackson.databind.ObjectMapper
import lv.intrum.homework.rest.ClientBasicInfo
import lv.intrum.homework.rest.ClientFullInfo
import lv.intrum.homework.rest.ClientRegistrationRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class ClientIntegrationTest extends Specification {

    @Autowired
    private MockMvc mvc

    @Autowired
    private ObjectMapper objectMapper

    def "should register client and return basic and detailed info"() {
        given: "client registration data"
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest();
        clientRegistrationRequest.email = "bob@riga.lv"
        clientRegistrationRequest.name = "Bob"
        clientRegistrationRequest.surname = "Jamil"
        clientRegistrationRequest.password = "Qwerty123"
        clientRegistrationRequest.country = "Latvia"

        when: "client submit data"
        def result = mvc.perform(post("/rest/v1/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRegistrationRequest)))
                .andExpect(status().isCreated())
                .andReturn()

        then: "client get basic info if registration success"
        def response = result.getResponse().getContentAsString()
        def createdClient = objectMapper.readValue(response, ClientBasicInfo.class)
        createdClient.name == "Bob"

        when: "Client try to login using his email and password"
        def authRequest = new AuthRequest()
        authRequest.email = "bob@riga.lv"
        authRequest.password = "Qwerty123"
        def loginResult = mvc.perform(post("/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(authRequest)))
                .andExpect(status().isOk())
                .andReturn()

        then: "client get token for login"
        def authResponse = loginResult.getResponse().getContentAsString()
        def tokenData = objectMapper.readValue(authResponse, AuthResponse.class)
        tokenData.email == "bob@riga.lv"


        when: "Client want to show his info"
        def firstInfoResult = mvc.perform(get("/rest/v1/client")
        .header("Authorization","Bearer " + tokenData.token))
                .andExpect(status().isOk())
                .andReturn()

        then: "Client see info about himself"
        def firstInfoResponse = firstInfoResult.getResponse().getContentAsString()
        def clientInfo = objectMapper.readValue(firstInfoResponse, ClientBasicInfo.class)
        clientInfo.name == "Bob"

        when: "Client want to show detailed info"
        def fullInfoResult = mvc.perform(get("/rest/v1/client/detailed")
                .header("Authorization","Bearer " + tokenData.token))
                .andExpect(status().isOk())
                .andReturn()

        then: "Client see info about himself"
        def fullInfoResponse = fullInfoResult.getResponse().getContentAsString()
        def clientFullInfo = objectMapper.readValue(fullInfoResponse, ClientFullInfo.class)
        clientFullInfo.name == "Bob"
        clientFullInfo.area == 64559
        clientFullInfo.population == 1961600
    }


}

class AuthResponse {
    String email, token
}

class AuthRequest {
    String email, password
}
