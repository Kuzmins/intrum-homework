package lv.intrum.homework.client

import lv.intrum.homework.countryresolver.CountryData
import lv.intrum.homework.rest.ClientRegistrationRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

@SpringBootTest
class ClientMapperTest extends Specification {
    @Autowired
    ClientMapper clientMapper

    @Autowired
    PasswordEncoder passwordEncoder

    def "mapper should encode password with Bcrypt"() {
        given: "client registration request"
        def testPass = "testPass"
        def registrationRequest = new ClientRegistrationRequest()
                .setName("client")
                .setPassword(testPass)

        def countryData = new CountryData()
                .setName("Some country");
        when:
        def client = clientMapper.requestToClient(registrationRequest, countryData)


        then:
        client.getPassword() != testPass
        passwordEncoder.matches(testPass, client.getPassword())

    }
}
