package lv.intrum.homework.client

import lv.intrum.homework.Success
import lv.intrum.homework.countryresolver.CountryData
import lv.intrum.homework.countryresolver.CountryResolver
import lv.intrum.homework.rest.ClientRegistrationRequest
import spock.lang.Specification

class ClientServiceTest extends Specification {
    def countryResolver = Stub(CountryResolver)
    def mapper = Mock(ClientMapper)
    def repository = Mock(ClientRepository)
    ClientService clientService = new ClientService(countryResolver, mapper, repository)

    private final static String TEST_COUNTRY = "Wakanda"
    private final static String TEST_EMAIL = "test@test.com"

    void setup() {
        repository.findByEmail(TEST_EMAIL) >> Optional.empty()
    }

    def "should throw exception if country resolver do not return counties or return more than one"() {
        given:
        ClientRegistrationRequest clientRegistrationRequest = testRequest()
        countryResolver.getCountryData(TEST_COUNTRY) >> resolvedCountry

        when:
        clientService.registerClient(clientRegistrationRequest)
        throw new Success()

        then:
        def error = thrown(expectedException)
        error.message == expectedMessage

        where:
        resolvedCountry                | expectedException     | expectedMessage
        []                             | WrongCountryException | "Wrong country name!"
        [testCountry(), testCountry()] | WrongCountryException | "Wrong country name!"
        [testCountry()]                | Success               | null
    }

    def "should throw Exception if client country is outside EU"() {
        given:
        ClientRegistrationRequest clientRegistrationRequest = testRequest()
        CountryData countryData = testCountry()
        countryResolver.getCountryData(TEST_COUNTRY) >> [countryData]
        countryData.setRegionalBlocks([regionalBlock])

        when:
        clientService.registerClient(clientRegistrationRequest)
        throw new Success()

        then:
        def error = thrown(expectedException)
        error.message == expectedMessage

        where:
        regionalBlock    | expectedException        | expectedMessage
        null             | IllegalArgumentException | "Country outside eu!"
        ""               | IllegalArgumentException | "Country outside eu!"
        "some block"     | IllegalArgumentException | "Country outside eu!"
        "European Union" | Success                  | null
    }

    def "mapped country should be persisted into db"() {
        given:
        ClientRegistrationRequest clientRegistrationRequest = testRequest()
        CountryData countryData = testCountry()
        countryResolver.getCountryData(TEST_COUNTRY) >> [countryData]
        DetailedClient mappedClient = new DetailedClient()
        mapper.requestToClient(clientRegistrationRequest, countryData) >> mappedClient

        when:
        clientService.registerClient(clientRegistrationRequest)

        then:
        1 * repository.save(mappedClient)
    }

    def "should throw exception if user with same email already exist"() {
        given:
        ClientRegistrationRequest clientRegistrationRequest = testRequest()
        clientRegistrationRequest.email = "bad@email.com"
        CountryData countryData = testCountry()
        countryResolver.getCountryData(TEST_COUNTRY) >> [countryData]

        repository.findByEmail(clientRegistrationRequest.email) >> Optional.of(new DetailedClient())

        when:
        clientService.registerClient(clientRegistrationRequest)

        then:
        def error = thrown(ClientAlreadyExistException)
        error.message == "Client with that email already exist!"
    }

    private static ClientRegistrationRequest testRequest() {
        new ClientRegistrationRequest().setCountry(TEST_COUNTRY).setEmail(TEST_EMAIL)
    }

    private static CountryData testCountry() {
        return new CountryData().setName(TEST_COUNTRY).setRegionalBlocks(["European Union"])
    }
}
