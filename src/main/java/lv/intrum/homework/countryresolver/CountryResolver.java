package lv.intrum.homework.countryresolver;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;


@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CountryResolver {

    private final RestCountryFetcher restCountryFetcher;
    private final RestCountryMapper restCountryMapper;


    public List<CountryData> getCountryData(String countryName) {
        Preconditions.checkArgument(StringUtils.isNotBlank(countryName), "CountryData name is mandatory");
        List<String> countryList = restCountryFetcher.getCountryList();

        if (!countryList.contains(countryName)) {
            return Collections.emptyList();
        }
        List<RestCountryResponse> responseList = restCountryFetcher.getCountryData(countryName);
        return restCountryMapper.responseToDate(responseList);

    }

    public List<String> getAvailableCountries() {
        return restCountryFetcher.getCountryList();
    }
}

