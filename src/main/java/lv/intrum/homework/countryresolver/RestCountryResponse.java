package lv.intrum.homework.countryresolver;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
class RestCountryResponse {
    private String name;
    private List<RegionalBlock> regionalBlocs;
    private Long population;
    private BigDecimal area;
    private List<String> borders;
}
