package lv.intrum.homework.countryresolver;

import lombok.Data;

@Data
class RegionalBlock {
    private String name;
    private String acronym;
}
