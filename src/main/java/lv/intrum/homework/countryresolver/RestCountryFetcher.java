package lv.intrum.homework.countryresolver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RestCountryFetcher {

    private final String restCountryUrl;

    private RestTemplate restTemplate;

    public RestCountryFetcher(@Value("${restcountry.url}") String restCountryUrl) {
        this.restCountryUrl = restCountryUrl;
        this.restTemplate = new RestTemplate();
    }

    public List<RestCountryResponse> getCountryData(String countryName) {

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(restCountryUrl).path("name/").path(countryName);

        try {
            return restTemplate.exchange(builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<RestCountryResponse>>() {
                    }).getBody();
        } catch (HttpClientErrorException.NotFound e) {
            return Collections.emptyList();
        }
    }

    public List<String> getCountryList() {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(restCountryUrl).path("all").queryParam("fields", "name");


        List<RestCountryResponse> countryData = restTemplate.exchange(builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<RestCountryResponse>>() {
                }).getBody();

        if (countryData != null) {
            return countryData.stream().map(RestCountryResponse::getName).collect(Collectors.toList());
        }

        return Collections.emptyList();
    }
}
