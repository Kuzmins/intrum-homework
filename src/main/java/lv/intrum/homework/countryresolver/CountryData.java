package lv.intrum.homework.countryresolver;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
public class CountryData {
    private String name;
    private List<String> regionalBlocks;
    private Long population;
    private BigDecimal area;
    private List<String> borders;
}
