package lv.intrum.homework.countryresolver;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface RestCountryMapper {

    @Mapping(source = "regionalBlocs", target = "regionalBlocks")
    CountryData responseToData(RestCountryResponse countryResponse);

    List<CountryData> responseToDate(List<RestCountryResponse> countryResponse);


    default List<String> blockToName(List<RegionalBlock> regionalBlock) {
        return regionalBlock.stream().map(RegionalBlock::getName).collect(Collectors.toList());
    }
}
