package lv.intrum.homework.rest;

import lv.intrum.homework.client.DetailedClient;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ClientRestMapper {

    @Mapping(source = "clientCountry.countryName", target = "country")
    @Mapping(source = "username", target = "email")
    ClientBasicInfo toClientBasicInfo(DetailedClient detailedClient);


    @Mapping(source = "clientCountry.countryName", target = "country")
    @Mapping(source = "username", target = "email")
    @Mapping(source = "clientCountry.area", target = "area")
    @Mapping(source = "clientCountry.borders", target = "borders")
    @Mapping(source = "clientCountry.population", target = "population")
    ClientFullInfo toClientDetailedInfo(DetailedClient detailedClient);
}
