package lv.intrum.homework.rest;

import lombok.Data;

@Data
public class ClientBasicInfo {
    private String name;
    private String surname;
    private String country;
    private String email;
}
