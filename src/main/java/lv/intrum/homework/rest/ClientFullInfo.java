package lv.intrum.homework.rest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ClientFullInfo extends ClientBasicInfo{
    private Long population;
    private BigDecimal area;
    private List<String> borders;
}
