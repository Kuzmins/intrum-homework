package lv.intrum.homework.rest;

import lv.intrum.homework.client.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping("/rest/v1/client")
@CrossOrigin(origins = "*")

public class ClientController {

    private final ClientService clientService;

    private final ClientRestMapper clientRestMapper;

    @Autowired
    public ClientController(ClientService clientService, ClientRestMapper clientRestMapper) {
        this.clientService = clientService;
        this.clientRestMapper = clientRestMapper;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ClientBasicInfo registerUser(@RequestBody @Valid ClientRegistrationRequest request) {
        DetailedClient detailedClient = clientService.registerClient(request);
        return clientRestMapper.toClientBasicInfo(detailedClient);
    }

    @GetMapping()
    public ClientBasicInfo getClientInfo(Authentication authentication) {
        Client client = (Client) authentication.getPrincipal();
        DetailedClient detailedClient = clientService.getDetailedClientByEmail(client.getUsername());
        return clientRestMapper.toClientBasicInfo(detailedClient);
    }

    @GetMapping("/detailed")
    public ClientFullInfo getClientDetailedInfo(Authentication authentication) {
        Client client = (Client) authentication.getPrincipal();
        DetailedClient detailedClient = clientService.getDetailedClientByEmail(client.getUsername());
        return clientRestMapper.toClientDetailedInfo(detailedClient);
    }


    @ExceptionHandler(WrongCountryException.class)
    public ResponseEntity<HashMap<String, Object>> wrongCountry(
            WrongCountryException e) {
        HashMap<String, Object> avaiableCountries = new HashMap<>();
        avaiableCountries.put("errorMessage", e.getMessage());
        avaiableCountries.put("availableCountries", e.getAvailableCountries());
        return new ResponseEntity<>(avaiableCountries, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ClientAlreadyExistException.class)
    public ResponseEntity<HashMap<String, Object>> alreadyExist(ClientAlreadyExistException e) {
        HashMap<String, Object> errors = new HashMap<>();
        errors.put("error", e.getMessage());
        return new ResponseEntity<>(errors, HttpStatus.CONFLICT);
    }
}
