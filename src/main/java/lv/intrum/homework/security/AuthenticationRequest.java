package lv.intrum.homework.security;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
class AuthenticationRequest {
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;
}
