package lv.intrum.homework.security;

class InvalidJwtAuthenticationException extends RuntimeException {

    InvalidJwtAuthenticationException(String s) {
        super(s);
    }
}
