package lv.intrum.homework.security;

import lv.intrum.homework.client.ClientRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class CustomUserDetailsService implements UserDetailsService {
    private ClientRepository users;
    public CustomUserDetailsService(ClientRepository users) {
        this.users = users;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        return this.users.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Username with email: " + email + " not found"));
    }
}