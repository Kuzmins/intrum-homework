package lv.intrum.homework.client;

import lv.intrum.homework.countryresolver.CountryData;
import lv.intrum.homework.rest.ClientRegistrationRequest;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
@DecoratedWith(ClientMapperDecorator.class)
public interface ClientMapper {
    @Mapping(source = "request.name", target = "name")
    @Mapping(source = "countryData.name", target = "clientCountry.countryName")
    @Mapping(source = "countryData.population", target = "clientCountry.population")
    @Mapping(source = "countryData.area", target = "clientCountry.area")
    @Mapping(source = "countryData.borders", target = "clientCountry.borders")
    @Mapping(source = "request.surname", target = "surname")
    DetailedClient requestToClient(ClientRegistrationRequest request, CountryData countryData);
}
