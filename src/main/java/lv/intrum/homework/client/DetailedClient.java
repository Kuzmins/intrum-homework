package lv.intrum.homework.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "CLIENT_TYPE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DetailedClient extends Client {
    private String name;
    private String surname;
    private ClientCountry clientCountry;
}
