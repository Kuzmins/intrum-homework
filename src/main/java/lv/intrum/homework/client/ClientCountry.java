package lv.intrum.homework.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.util.List;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientCountry {
    private String countryName;
    private Long population;
    private BigDecimal area;
    @ElementCollection
    @CollectionTable(name = "country_borders")
    @Column(name = "border")
    private List<String> borders;

}
