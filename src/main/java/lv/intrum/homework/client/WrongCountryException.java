package lv.intrum.homework.client;

import java.util.List;

public class WrongCountryException extends RuntimeException {

    private final List<String> availableCountries;

    public WrongCountryException(String s, List<String> availableCountries) {
        super(s);
        this.availableCountries = availableCountries;
    }

    public List<String> getAvailableCountries() {
        return availableCountries;
    }
}
