package lv.intrum.homework.client;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lv.intrum.homework.countryresolver.CountryData;
import lv.intrum.homework.countryresolver.CountryResolver;
import lv.intrum.homework.rest.ClientRegistrationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ClientService {

    private final CountryResolver countryResolver;
    private final ClientMapper clientMapper;
    private final ClientRepository clientRepository;

    @Transactional
    public DetailedClient registerClient(@Valid ClientRegistrationRequest clientRegistrationRequest) {
        checkClientEmail(clientRegistrationRequest.getEmail());

        List<CountryData> countryDataList = countryResolver.getCountryData(clientRegistrationRequest.getCountry());
        checkCountry(countryDataList);

        CountryData countryData = countryDataList.get(0);
        Preconditions.checkArgument(countryData.getRegionalBlocks().contains("European Union"), "Country outside eu!");

        DetailedClient detailedClient = clientMapper.requestToClient(clientRegistrationRequest, countryData);
        clientRepository.save(detailedClient);
        return detailedClient;
    }

    @Transactional
    public DetailedClient getDetailedClientByEmail(String email) {
        Optional<DetailedClient> client = clientRepository.findByEmail(email);
        return client.orElseThrow(() -> new ClientNotFoundException(email));
    }

    private void checkCountry(List<CountryData> countryDataList) {
        if (countryDataList.size() != 1) {
            throw new WrongCountryException("Wrong country name!", countryResolver.getAvailableCountries());
        }
    }

    private void checkClientEmail(String email) {
        clientRepository.findByEmail(email)
                .ifPresent(detailedClient -> {
                    throw new ClientAlreadyExistException("Client with that email already exist!");
                });
    }
}
