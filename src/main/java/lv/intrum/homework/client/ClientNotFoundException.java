package lv.intrum.homework.client;

import lombok.Getter;

@Getter
public class ClientNotFoundException extends RuntimeException {

    private final String clientEmail;

    ClientNotFoundException(String clientEmail) {
        this.clientEmail = clientEmail;
    }
}
