package lv.intrum.homework.client;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<DetailedClient, Long> {
    Optional<DetailedClient> findByEmail(String email);
}
