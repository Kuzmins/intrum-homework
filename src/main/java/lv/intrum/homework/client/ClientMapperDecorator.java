package lv.intrum.homework.client;

import lv.intrum.homework.countryresolver.CountryData;
import lv.intrum.homework.rest.ClientRegistrationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

public abstract class ClientMapperDecorator implements ClientMapper {

    @Autowired
    @Qualifier("delegate")
    private ClientMapper delegate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public DetailedClient requestToClient(ClientRegistrationRequest request, CountryData countryData) {
        DetailedClient detailedClient = delegate.requestToClient(request, countryData);
        detailedClient.setPassword(passwordEncoder.encode(request.getPassword()));
        detailedClient.setRoles(Collections.singletonList("USER"));
        return detailedClient;
    }
}
