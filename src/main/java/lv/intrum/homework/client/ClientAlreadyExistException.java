package lv.intrum.homework.client;

public class ClientAlreadyExistException extends RuntimeException {
    public ClientAlreadyExistException(String s) {
        super(s);
    }
}
